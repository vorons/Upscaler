# file_chooser.py: file chooser dialogs for opening and outputting files
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from os.path import basename
from gi.repository import Gtk, Gio, GdkPixbuf, GLib
from upscaler.filters import get_format_filters, supported_filters, image_filters
from gettext import gettext as _


class FileChooser:

    @staticmethod
    def __load_image_done(_obj, result, data):
        callback_good = data[0][0]
        callback_bad = data[0][1]
        input_file_path = data[1]

        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_stream_finish(result)
        except GLib.Error as error:
            print(f"Unable to load image, {error}")
            callback_bad(error)
            return

        callback_good(input_file_path, pixbuf)

    @staticmethod
    def __open_file_done(file, result, callbacks):
        callback_bad = callbacks[1]

        try:
            input_stream = file.read_finish(result)
        except GLib.Error as error:
            print(f"Unable to open file, {error}")
            callback_bad(error)
            return

        GdkPixbuf.Pixbuf.new_from_stream_async(input_stream,
                                               None,
                                               FileChooser.__load_image_done,
                                               (callbacks, file.get_path()))

    """ Load a given file. """
    @staticmethod
    def load_file(file, callback_good, callback_error):
        print(f"Input file: {file.get_path()}")
        file.read_async(GLib.PRIORITY_DEFAULT,
                        None,
                        FileChooser.__open_file_done,
                        (callback_good, callback_error))

    """ Open and load file. """
    @staticmethod
    def open_file(parent, previous_file_path, callback_good, callback_error, *args):
        def load_file(_dialog, response):

            """ Return if user cancels. """
            if response != Gtk.ResponseType.ACCEPT:
                callback_error(None)
                return

            """ Run if the user selects an image. """
            file = dialog.get_file()

            """ Return if the previous and current chosen images are the same. """
            if previous_file_path == file.get_path():
                return

            parent.start_loading()

            FileChooser.load_file(file, callback_good, callback_error)

        dialog = Gtk.FileChooserNative.new(
            title=_('Select an image'),
            parent=parent,
            action=Gtk.FileChooserAction.OPEN
        )
        dialog.set_modal(True)
        dialog.connect('response', load_file)
        dialog.add_filter(supported_filters())
        dialog.show()

    """ Select output location. """
    @staticmethod
    def output_file(parent, default_name, callback_good, callback_bad, *args):
        def upscale_content(_dialog, response):

            """ Set output file path if user selects a location. """
            if response != Gtk.ResponseType.ACCEPT:
                callback_bad(None)
                return

            """ Get all filters. """
            filters = []
            for filter in get_format_filters('image'):
                filters.append(filter.split('/').pop())

            """ Check if output file has a file extension or format is supported. """
            if '.' not in basename(dialog.get_file().get_path()):
                callback_bad(_('No file extension was specified'))
                return

            elif basename(dialog.get_file().get_path()).split('.').pop().lower() not in filters:
                filename = basename(dialog.get_file().get_path()).split('.').pop()
                callback_bad(_(f'’{filename}’ is an unsupported format'))
                return

            output_file_path = dialog.get_file().get_path()
            print(f'Output file: {output_file_path}')
            callback_good(output_file_path)

        dialog = Gtk.FileChooserNative.new(
            title=_('Select output location'),
            parent=parent,
            action=Gtk.FileChooserAction.SAVE
        )
        dialog.set_modal(True)
        dialog.connect('response', upscale_content)
        dialog.add_filter(image_filters())
        dialog.set_current_name(default_name)
        dialog.show()
